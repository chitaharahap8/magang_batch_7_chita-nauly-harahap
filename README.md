# Panduan Penggunaan Git

## Download Git
Pastikan anda telah memiliki Git, jika belum dapat diunduh di [sini](https://git-scm.com/downloads)

## Unduh Repository
Cara mengunduh Repository dengan cara paste link repository yang akan Anda clone, menggunakan perintah 'git clone'

```
git clone <url repository>
```

## Membuat Branch 
Anda diharuskan membuat branch baru agar tidak merusak branch master yang sudah stable, cara membuat branch seperti dibawah ini.

```
git branch <name branch>
```

## Checkout Menuju Branch baru
Agar dapat mengirim pada repository tersebut diharuskan untuk checkout terlebih dahulu.

```
git checkout <name branch>
```

## Konfirmasi Perubahan

```
git add .
```
```
git commit -m <nama file baru>
```

## Kirim Perubahan ke Repository
Langka terakhir yaitu dengan mengirim perubahan tersebut (file terbaru yang digunakan pada git commit).

```
git push origin <name branch>
```
